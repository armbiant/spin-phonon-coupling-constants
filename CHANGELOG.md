# Changelog

<!--next-version-placeholder-->

## v0.15.0 (2023-02-22)
### Feature
* Enable non-primitive unit cells in phonon calc ([`2f40fc7`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/2f40fc7e85085327c88ac8672f0ba9d812494506))

## v0.14.0 (2023-02-21)
### Feature
* Flexible mass argument ([`9ca06da`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/9ca06da045ac6256dcffe19b497d5f11a5def712))

## v0.13.0 (2023-02-20)
### Feature
* Implement new lvc interface ([`7fea3c1`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/7fea3c1ec6a71c99141dab362f1f5e5b06ce7b57))
* Hpc style properties, wip ([`1cd715e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/1cd715eaa2cd58d6644c4019456cd4df17b8b986))

## v0.12.1 (2023-01-30)
### Fix
* Allow for none integer atomic masses ([`6ce2b00`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/6ce2b00a4e8ea30227e92950847e941fe6bf83e5))

### Documentation
* Change docs to be consistent with updated molcas_suite and spin_phonon_suite ([`2d8a203`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/2d8a203387b98cc8c972de1e9634e12a0554393a))
* Add cleaning of local scratch ([`fe04264`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/fe04264babf50e361a0a09747ad313caf23b33cc))
* Fix typo in local scratch path ([`0288038`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/0288038fd8b9b8b46940a0acfa09c570ad2006aa))

## v0.12.0 (2022-11-04)
### Feature
* Make basis set definition less redundant ([`18d184e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/18d184e9a752234d6c54485a1e81c592e72222b6))

### Documentation
* Updated docs ([`33f6571`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/33f6571e25e295224a80510b9f32ede0d22402c5))

## v0.11.0 (2022-10-10)
### Feature
* Pyvibms output ([`112692e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/112692ed57f32abd7b18eb95b03b86e65f90d606))

### Documentation
* Update docs to use local copy ([`0fa4ba3`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/0fa4ba34436b6e780975f69005fa18206bbef707))

## v0.10.1 (2022-09-29)
### Fix
* Minor bug fix ([`a51a3ee`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/a51a3ee2dc2ea021420e02792879abf9eebe8c0b))

## v0.10.0 (2022-09-29)
### Documentation
* Update documentation ([`89a6ad4`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/89a6ad4529d95ee2ec92047c844e22ae70a80381))

## v0.9.1 (2022-09-22)
### Fix
* Add xyz file number of atoms and comment lines ([`2c0d8c7`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/2c0d8c7569024ee18638b644471636a788398987))

### Documentation
* Update gen_charges sample output ([`e2044a2`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/e2044a28352622177deb46cce23fd00f3a7e3e0a))
* Fix code indentation ([`cf8a232`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/cf8a23279fdf2f0c96837eb3f89a108f7c2be6df))

## v0.9.0 (2022-09-01)
### Feature
* Add vis_charges cli arg ([`96ec363`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/96ec3636a07232c6e28e10486718faeb21051209))
* Add charges viewer code ([`dbb3ffa`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/dbb3ffa7d1f24925a6ee8ed641ecb82d74c10cb1))
* Add charge visualisation ([`a9e7d33`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/a9e7d33d2e63fbd0797c52031ce73ca6d1827f1a))
* Add visualisation ([`07bf1f3`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/07bf1f3c8e72147e4bd8ef1b18e3ce4212adffd9))

### Documentation
* Update environment page to be clearer and add note on visualisation ([`b4d0e46`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/b4d0e465c5ff8659a4381ff33baafc401c245e0e))

## v0.8.0 (2022-09-01)
### Feature
* Add visualisation options to parse_charges ([`c096e9f`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/c096e9f82f65daabe846cdadc1bd915d3c99efbe))

## v0.7.1 (2022-08-31)
### Documentation
* Update equilibrium section for charges, and names of files in environment section ([`e457a66`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/e457a665fdf1a445bf7705e8654724561a23e6b4))
* Change basis set specification file to ENV, add comment, and update help ([`765c102`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/765c102d04897fb57ab521814c7f948055a5ac79))

## v0.7.0 (2022-08-26)
### Feature
* Add molcas basis set output for environment charges ([`254956c`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/254956ca82e7fe54695366632062726b540abaa6))

### Documentation
* Add information on output files of parse_charges ([`204e6c1`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/204e6c1ad844ae05f57a586c5f02d89b5128d336))
* Add cells module ([`7f64c4c`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/7f64c4c7aeca87e8a1f39902e6546324ee22eae2))
* Add deletion of old docs badge ([`b3f848b`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/b3f848bd0c83eb8eede4167ddca57d0224f6e493))

## v0.6.0 (2022-08-26)
### Feature
* Add --force_expansion arg to indicate phonon supercell expansion ([`1acc876`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/1acc876d8ed8cfbbe8d382df9bc60a9ca7881c11))

### Documentation
* Add note for forces supercell expansion ([`174dca8`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/174dca8ae59f30fbea4c2a4d7cb115e84bcce1d4))

## v0.5.0 (2022-08-25)
### Feature
* Readd printing of supercell_charges.dat ([`3a26160`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/3a261600939c9a8c9975eb5477c988c209302aa9))

## v0.4.1 (2022-08-25)
### Fix
* Name of cli interface with underscores ([`6d87e79`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/6d87e79f374b0622eb1b52f0a10b9eb8fc18eac6))

## v0.4.0 (2022-08-25)
### Feature
* Polish phonopy interface ([`c2ae306`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/c2ae3069f695084ba68ae6bc026bc6bfc0b3fbaf))
* Commensurate q points now implemented ([`9bed548`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/9bed548a641536260783063f0731d13051f834c6))
* Work on phonopy interface, wip ([`23327ed`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/23327edd985be76b3f09b2a91d8301171dc5b63b))
* Add phonopy interface ([`4c5fd87`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/4c5fd87d2c821294682eff2d205560df2e587c44))

### Fix
* Rename cli interface to be spin-phonon_suite ([`4279835`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/42798351959d9fba81c9ef07521273bff064a013))
* Clean up phonopy interface ([`f26e781`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/f26e7812f6542d88382c19938c42cb27503caed4))

### Documentation
* Improve documentations ([`83cdfb1`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/83cdfb192221427d34e2f420bfd855bd24731c72))
* Update environment ([`f17b53f`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/f17b53f196bda9d4df74e59405fa722b714cfa05))
* Update environment ([`78b68c8`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/78b68c8303471b44a5fbc39a291e332d50a05fee))
* Rearrange environment step ([`41b5adf`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/41b5adfe7d866431b5069bee702c888f58900f5d))
* Fix formatting ([`59a1e28`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/59a1e28c9d734f3d9f4ac89a678b927b185d5f41))

## v0.3.1 (2022-08-23)
### Fix
* Remove merge conflict ([`3c43f0b`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/3c43f0bf58dc483ca521f9fc940906a88f8e39a0))

## v0.3.0 (2022-08-23)
### Feature
* Add repaired xyz file printing ([`b34a026`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/b34a026bed69aacbb7b15eff555f0eaf9953419d))
* Add warning for high symmetry systems ([`75aebbb`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/75aebbb612f8a1ec9de94e0b7506248ee8eaf9c1))
* Add new basis set and ecp defs ([`60b554f`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/60b554f15c56eeae1595aa5003d4b115fe055d0f))
* Add supercells class and remove use of phonopy for supercell construction ([`3ca112a`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/3ca112a96c197e0d4bc6e78956b336f6dca54b93))
* Add supercell charges code ([`5afff8a`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/5afff8aedea65a4bd0d45f560e0b55235d2cf233))

### Fix
* Add missing expansion attribute to SuperCell ([`cbd1a32`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/cbd1a3284c59e64e3aaed66625d472081d87413d))
* Correct bug in lattice vector row vs column major ([`03cafda`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/03cafda67562a3154c481b72d2ebfef0929564bc))
* Add hydrogen atom to basis set defs ([`fef473a`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/fef473a619c3be788c2a11ddac8fafa92201ba52))

### Documentation
* Update docstrings ([`4f047b1`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/4f047b1e85ea55e7906fdc60551b215601b8c31a))
* Update help for parse_charges ([`3422d9e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/3422d9ee58539f7de80f7b6408cc4885e184d9bb))
* Add description to charge subparsers ([`2fc7011`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/2fc701164f06bdeee0f5986f6e6206c5f017085f))

### Performance
* Replace old frac --> cart conversion ([`dbe52ea`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/dbe52ea8931f2e6b291dc8ad319488193fc8a055))
* Use mic distances to check for repairs ([`e8e2fc6`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/e8e2fc67bf8b6fdf223b021c4bd68d127e72d062))
* Simplify file names and handling ([`533b02d`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/533b02d7bd531ce8a3b581591b305c1aaa2b65e1))

## v0.2.0 (2022-08-23)
### Feature
* Adjust phases ([`9e333f7`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/9e333f79735797a05d93fe65776df6dc2fb37c6c))

### Documentation
* Add coming soon ([`e6e499d`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/e6e499dbece35f8127045012010b013284a7e9f0))
* Correct grammar ([`28d735e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/28d735e5b4eee8118b041ac855447f91c803b3d4))
* Indentation error ([`f6dbc2c`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/f6dbc2c2abc0f87a1aef7b5d22f6c99c60f8b757))
* Update environment docs ([`d9d3b91`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/d9d3b9179fed45bc605a36ae72132d677cb37a5d))
* Add environment guide ([`4e76bd2`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/4e76bd28791625b8f6a1fc0f022f965f88df44de))

## v0.1.1 (2022-08-19)
### Documentation
* Add sphinx based documentation ([`b92c5e1`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/b92c5e10e3348d1a81b4dabfd9001801e648de41))

## v0.1.0 (2022-08-19)
### Feature
* Add spin phonon cupling strength plotting ([`d8c4a65`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/d8c4a65c0fd846a1a7f350de7060d336ff3ec249))

## v0.0.2 (2022-06-21)
### Fix
* Active atom indexing ([`016884d`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/016884dae85e85b2043fc110e9004d5e5e60c490))

## v0.0.1 (2022-06-09)
### Feature
* Add max_analytic_order argument ([`efa7dcc`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/efa7dcc13746e3c8d6cffcd828597d10cbc2191f))
* Add progress print out ([`0ab266c`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/0ab266cb375a5407dd1d079ab606f55be7bf48cb))
* Add max_analytic_order argument ([`7da26f0`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/7da26f05d34108422d68496feeacff50ccffc1f8))
* Add progress print out ([`37f9afd`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/37f9afdb6fc76149a0f7be8d15069340bb48ecf4))
* Support of active atoms in the coupling calculation ([`aa7a94c`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/aa7a94cff6dac1633119c58df6457c8fa8ea843b))
* Gradient norm and invariance printing ([`46def28`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/46def28acdb81b268abc26ab6b26e2853f727ff2))
* Implement prep tau part ([`99cbc8e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/99cbc8e90b33b9ed15087d5fdd55e9cb411150ca))
* Enable choice to project out translations and rotations ([`00a4b59`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/00a4b59209ccd42d729f3aa18a235e83b84b554d))
* Setup of vibration database ([`d6a4ae3`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/d6a4ae3bdd5f862b145ff62be520cfdede859053))
* Add hessian matrix diagonalisation code ([`6406e2c`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/6406e2c224285bc81883954b66a146b15ea14d95))
* Add hessian diagonalisation code for force constants ([`0795e13`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/0795e138aade65752aa21aac5905e93b10b0144d))
* Add alaska options for lvc inputs ([`ce85501`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/ce8550103005679f4701b83ea66378c19cb4b36a))
* Add mclr option ([`6e300c0`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/6e300c08da93e52d15b6dd63cac76a3e80e56667))
* Add user argument for number of rassi states replacing hard coded Dy ([`947d66d`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/947d66d520daa2bc7ebf731b20fef0ad40eca7db))
* Add multiplicity to `molecules` argument ([`3de7960`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/3de79607d1a236dcc8bc3fda18e40c6e69a415a6))

### Fix
* Correct handling of max_order arguments ([`76826b8`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/76826b875b864d412eaefc45f54e05f0e870087c))
* Minor updates ([`ce42f1b`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/ce42f1bb57679248392921c575c6abab85ac0fb0))
* Input_generator bug ([`220339a`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/220339a4f178065052af36db18b63aba465a0e5b))
* Minor updates ([`39b96aa`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/39b96aa4cb3d06226eabdf7ce21e29c892bde002))
* Input_generator bug ([`bcf8060`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/bcf80603502c55ebef9a8c6776e0d71b985c7345))
* Verbose matrix printing bug ([`70a304a`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/70a304a388775fd1b4aea0ca18cdffce46032d20))
* Bug-fixes in handling of distortions ([`68ce593`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/68ce59394d4db4ca87afe2d18bf85552094fdc77))
* Bug-fix in input file generation ([`4cb5f9b`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/4cb5f9bdcde3a635cc47b79a4165b78d3735a278))
* Correction in sf2ws_spin ([`35d158e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/35d158e3853146b231f9efb1193f72648eb0343a))
* Improvement in LVC/CFP code ([`d8d040b`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/d8d040b3de0193fa2ddadaa16fe20c297be45931))
* Fix of phasing and block ordering issues ([`adb79b0`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/adb79b024a71a1ebf771e6f6aa37447df975c8a4))

### Breaking
* change in interface ([`c5513de`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/c5513decadd209b8855b3a8f14f65f7912b1273f))

### Documentation
* Pep8 compliance ([`153965a`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/153965a50d9c2547f9178f98e7ecbb106eb86a88))
* Remove incorrect comment about Dy hard coding ([`adde5ce`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/adde5ce0c5de0fbf02131e76afb4a20d7b0d9a46))
* Add missing backtick ([`c653bf2`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/c653bf2c92e017ee46699bcf294d25660c37404c))
* Update readme with new suite installation methods ([`279932b`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/279932b6370163f12446f5423c0d3728d4fae9b3))
