Environment
===========

In spin_phonon_suite, the environment around the central SMM (cations, anions, solvent) is represented
by point charges as opposed to molecular orbitals (MO). This is done to facilitate
faster and cheaper calculation of the spin-phonon coupling, and has been shown
to only result in a minor accuracy penalty over expensive full MO calculations. :sup:`[1]`

Two types of environments can be studied in spin_phonon_suite:
    1. Periodic environments which are highly ordered and regular, e.g. crystalline lattices and surfaces
    2. Amorphous environments which are essentially random and irregular e.g. frozen solutions or powders

Periodic (ordered) environments
-------------------------------

Here, optimisation and frequency calculations are carried out on a unit cell
using periodic density functional theory (DFT). This unit cell is then translated
multiple times and used to build a supercell in which the spin-phonon coupling is
studied.

Owing to its highly ordered nature, this supercell will contain many copies of
the same molecules/ions, and so there it is most sensible to calculate charges for
the unique entities and then map these onto every occurrence of a given entity.

In ``spin_phonon_suite`` we use the CHarges from ELectrostatic Potentials using
a Grid-based method (CHELPG) process to determine charges that represent the
DFT molecular electrostatic potential of a given molecule in the supercell.
The interaction between entities in the supercell is neglected in our approach,
and so the CHELPG charges are determined for isolated entities.

Step 1: Calculating CHELPG charges
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``gen_charges`` subprogram available in the ``spin_phonon_suite`` command
line interface takes an optimised cell (in the form of a ``POSCAR`` file,
performs a user specified supercell expansion, and finds the unique entities
in the supercell by finding the symmetrically independent atoms that can be used
to rebuild the supercell according to the symmetry elements of the relevent space
group. This information is then stored in a pickled python object, ready to be used
in step 2.

e.g. for a (3,3,3) supercell expansion for a Dysprosocenium system with no
solvent

.. code:: text

    spin_phonon_suite gen_charges POSCAR 3 3 3

results in the following terminal output

.. code:: text

    Supercell written to supercell.xyz
    ******************************
    The unique entities are
    C34H58Y_1
    BC24F20_1
    ******************************
    Unique entities written to unique_entities.xyz

    Input file written to gaussian/C34H58Y_1.com
    Generate submission script with gaussian_suite gen_job gaussian/C34H58Y_1.com NCORES 

    Input file written to gaussian/BC24F20_1.com
    Generate submission script with gaussian_suite gen_job gaussian/BC24F20_1.com NCORES 

    Charge and multiplicity set to 99 in all .com files
    Please replace with actual values before submitting  

    Supercell object pickled in supercell.pickle

For each unique entity a Gaussian input (``.com``) file has been produced
specifying a single CHELPG calculation. Users MUST edit each ``.com`` file
to specify the charge and spin multiplicity of each entity.

By default the CHELPG calculations use PBE+D3 with Stuttgart 1997 ECPs for
metal ions, so some user configuration may be needed if this default is not desired.

Additionally, if closed shell analogues are to be used, e.g. Dy --> Y, then this swap
can either be carried out by editing the ``POSCAR``, or by editing the ``.com`` file.

When studying high symmetry systems, it is likely that the symmetrically independent
atoms will not constitute a molecule on their own. Consider a perfectly tetrahedral
coordination complex, only a single ligand and metal ion is needed to reproduce the
entire structure. This symmetry *should* be reflected by the CHELPG charges which
are carried out on the entire molecule, provided Gaussian employs the relevant point
group in the calculation. However, this is not guaranteed, and so `gen_charges` informs the user
that they should check that symmetry is obeyed in the output (`.log`) file of the relevant
calculation(s).

Step 2: Mapping CHELPG charges on to the supercell
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Following the successful calculation of the CHELPG charges, the ``parse_charges``
subprogram can be used to map the unqiue entities back onto those of the supercell.
This step must be run in the same directory as ``gen_charges``, containing both 
the ``supercell.pickle`` file, and the ``gaussian`` directory which itself contains
the relevant ``.log`` files.

e.g. following on from the above example

.. code:: text

    spin_phonon_suite parse_charges C34H58Y Y 

where the central SMM entity and metal ion must be explicitly stated. This is
required as the supercell is shifted periodically such that an
SMM entity is positioned at the centre of the supercell, avoiding an
unbalanced environment around the SMM. 

Five files are created by ``parse_charges``
    1. ``central_o_shifted.xyz`` - Central SMM coordinates
    2. ``environment_o_shifted.xyz`` - Environment coordinates
    3. ``environment_charges.dat`` - Environment charges
    4. ``ENV`` - Custom molcas basis set specification for environment charges
    5. ``supercell_o_shifted.xyz`` - Molcas XYZ file with predefined environment basis set labels

Of these, 4 and 5 are used to specify the system coordinates and environment charges in the Equilibrium and Coupling steps.

Optionally, the ``--vis_charges {all, supercell, entities}`` argument can be provided to ``parse_charges``
which generates ``.html`` files that visualise the charges by superimposing a color scale on
the atoms of either the individual entities or entire supercell.

Amorphous Environments
----------------------

Coming soon!


References
----------

[1] J. Staab and N. Chilton, ChemRxiv, 2022, doi:10.26434/chemrxiv-2022-kc0k3 
