Calculating Coupling Parameters
===============================

If spin-phonon couplings are evaluated via the linear vibronic coupling (LVC)
method, molecular gradients and nonadiabatic couplings (NACs) have to be
calculated with Molcas. Note, this calculation uses files located in the scratch
directory of the equilibrium calculation which is specified by the ``--old_path``
argument.

.. code:: bash

   # Generate lvc_rootNN.input Molcas inputs
   spin_phonon_suite generate_input eq --ion Dy --old_path ~/scratch/_MY_SMM_ --mclr_extra CHOF

   # Generate job array with local scratch
   molcas_suite generate_job '${STEM}.input' --scratch '/tmp/users/${USER}/_MY_SMM_/${STEM}' \
    --molcas_module apps/gcc/openmolcas/22.06 --node_type high_mem --disk 100000 --exit_code 'rm -r ${WorkDir}' \
    --array lvc_root01 lvc_root02 lvc_root03 lvc_root04 lvc_root05 lvc_root06 lvc_root07 lvc_root08 lvc_root09 lvc_root10 lvc_root11 lvc_root12 lvc_root13 lvc_root14 lvc_root15 lvc_root16 lvc_root17 lvc_root18 \
    --job_name lvc --submit_file submit.job

Note:
    * Add ``#$-l tmpdir_free=100G`` to the SGE header of ``submit.job`` to ensure sufficient local scratch disk space.

Next, two HDF5-databases are generated:
    * The coordinate basis in which the derivatives will be calculated (``-D``)
    * The LVC parametrisation (``-L``)

.. code:: bash

   # Define coordinate basis
   spin_phonon_suite init -D coordinates.hdf5 --num_atoms 93 --atomic 1-93

   # Generate LVC model
   spin_phonon_suite lvc -L lvc.hdf5 -D coordinates.hdf5 --rassi eq.rassi.h5 \
        --grad lvc_root01.out lvc_root02.out lvc_root03.out lvc_root04.out lvc_root05.out lvc_root06.out \
        lvc_root07.out lvc_root08.out lvc_root09.out lvc_root10.out lvc_root11.out lvc_root12.out \
        lvc_root13.out lvc_root14.out lvc_root15.out lvc_root16.out lvc_root17.out lvc_root18.out

Finally, input files for ``tau`` are prepared:

.. code:: bash

   # Computation of spin-phonon coupling parameters (CFP derivatives)
   spin_phonon_suite prep tau -L lvc.hdf5 -V vibrations.hdf5 -D coordinates.hdf5 --ion Dy3+ --basis j --quax quax.hdf5

   # The QUAX argument is optional, if used quax have to be extracted from eq.out or provided otherwise
   molcas_suite extractor -i eq.out -o quax.hdf5 --quax j

This prepares:
    * ``mode_energies.dat``, ``EQ_CFPs.dat`` and ``CFP_polynomials.dat``
