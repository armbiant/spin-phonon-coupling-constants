Equilibrium electronic structure
================================

The equilibirum electronic structure describes the system part of the
spin-phonon coupled system. It is most commonly computed at the CASSCF
level of theory.

Gas-phase calculations
----------------------

To generate an equilibrium input file for a gas-phase calculation,
i.e. without an environment, use `molcas_suite`, e.g.

.. code:: bash

   # Generate input
   molcas_suite generate_input opt.xyz Dy 9 7 10 +1 --output eq.input --high_S_only --skip_magneto --decomp RICD_acCD

   # Generate job submission file
   molcas_suite generate_job eq.input --scratch ~/scratch/_MY_SMM_ --molcas_module apps/gcc/openmolcas/22.06 --submit_file eq.job


Including environment charges
-----------------------------

To generate an equilibrium input file which includes charges that represent the
environment, use `molcas_suite` with the ``--gateway_extra`` optional arguments
which specify the coordinates and definitions of the charges respectively from
the environment step, e.g.

.. code:: bash

   # Generate input 
   molcas_suite generate_input supercell_o_shifted.xyz Dy 9 7 10 +1 --output eq.input --high_S_only --skip_magneto --gateway_extra 'basdir=$CurrDir' --decomp RICD_acCD

   # Generate job submission file
   molcas_suite generate_job eq.input --scratch ~/scratch/_MY_SMM_ --molcas_module apps/gcc/openmolcas/22.06 --submit_file eq.job

Note:
    * Do not forget to revert subsitutions by diamagnetic analogues, e.g.
      Dy -> Y. This applies to all instances in both ENV and coordinate file.
    * The index of the central metal centre (``1`` in the example) has to be
      determined from the ``supercell_o_shifted.xyz`` coordinate file and
      inserted into the ``molcas_suite generate_input`` command. Generally it
      is the only one without a predefined ``ENV.NNN`` basis label. 
    * Even though your SMM might carry a charge, condensed state systems should
      have a net charge of zero. Hence, this is the charge passed to
      ``molcas_suite generate_input``.
    * Due to the potentially high number of environment atoms, you might need
      to use a custom compiled version of ``OpenMolcas`` with increased
      ``mxAtom``, ``Mxdbsc`` and ``MxShll`` limits.
