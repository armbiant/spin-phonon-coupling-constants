Coupling Strength
=================

The coupling strength is defined as 

.. math ::
    S = \left[\frac{1}{3}\sum_{k}\frac{1}{2k+1}\sum_{q = -k}^{k}|B_{k,q}|^{2}\right]^{\frac{1}{2}}

where :math:`B_{k,q}` is a Wybourne crystal field parameter (CFP), and :math:`|B_{k,q}|` is defined as 

.. math ::
    |B_{k,q}| &= \sqrt{\text{Re}(B_{k,q})^2+\text{Im}(B_{k,q})^2}\\
